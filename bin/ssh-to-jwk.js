#!/usr/bin/env node
'use strict';

var fs = require('fs');
var path = require('path');
var sshtojwk = require('../index.js');

var pubfile = process.argv[2];
var pub = process.argv[3];

if (!pubfile) {
  pubfile = path.join(require('os').homedir(), '.ssh/id_rsa.pub');
}

var buf = fs.readFileSync(pubfile);
var txt = buf.toString('ascii');
var opts = { public: 'public' === pub };
var ssh;

if ('-' === txt[0]) {
  opts.pem = txt;
} else {
  opts.pub = txt;
}

ssh = sshtojwk.parse(opts);

// Finally! https://superuser.com/a/714195
sshtojwk.fingerprint(ssh).then(function (fingerprint) {
  console.warn('The key fingerprint is:\n' + fingerprint + ' ' + ssh.comment);
  console.info(JSON.stringify(ssh.jwk, null, 2));
});
