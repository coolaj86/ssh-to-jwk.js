'use strict';

// The purpose of this module is to abstract away
// the parts that aren't vanilla js (for easy portability)
// and to work with native JavaScript Uint8Arrays

var Enc = module.exports;

Enc.base64ToBuf = function (str) {
  return Buffer.from(str, 'base64');
};

Enc.base64ToHex = function (str) {
  return Buffer.from(str, 'base64').toString('hex');
};

Enc.base64ToUrlBase64 = function (b64) {
  return b64.replace(/\+/g, '-').replace(/\//g, '_').replace(/=/g, '');
};

Enc.bnToUrlBase64 = function (bn) {
  var hex = bn.toString(16);
  if (hex.length % 2) { hex = '0' + hex; }
  return Enc.base64ToUrlBase64(Buffer.from(hex, 'hex').toString('base64'));
};

Enc.bufToBase64 = function (u8) {
  return Buffer.from(u8).toString('base64');
};

Enc.bufToBin = function (u8) {
  return Buffer.from(u8).toString('binary');
};

Enc.bufToHex = function (u8) {
  return Buffer.from(u8).toString('hex');
};

Enc.bufToUrlBase64 = function (u8) {
  return Enc.base64ToUrlBase64(Enc.bufToBase64(u8));
};
